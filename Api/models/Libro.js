'use strict';

const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './storage/database.sqlite'
});

const Libro = sequelize.define('Libro', {
    id: {
        primaryKey: true,
        type: Sequelize.BIGINT,
        autoIncrement: true
    },
    titulo: {
        type: Sequelize.STRING
    },
    descripcion: {
        type: Sequelize.STRING
    },
    autor: {
        type: Sequelize.STRING
    },
    editorial: {
        type: Sequelize.STRING
    }
});

Libro.sync();

module.exports = Libro;