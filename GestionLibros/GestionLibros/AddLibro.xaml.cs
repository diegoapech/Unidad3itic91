﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestionLibros.Data;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GestionLibros
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddLibro : ContentPage
    {
        private LibroManager manager;
        public AddLibro(LibroManager manager)
        {
            InitializeComponent();
            this.manager = manager;
        }

        public async void OnSaveLibro(object sender, EventArgs e)
        {
            await manager.Add(txtTitulo.Text, txtDescripcion.Text, txtAutor.Text, txtEditorial.Text);
        }
    }
}