﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestionLibros.Data
{
    public class Libro
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string Autor { get; set; }
        public string Editorial { get; set; }

        public string Info => "ID: " + Id + "  -  " + Titulo;
    }
}
