﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GestionLibros.Data
{
    public class LibroManager
    {
        const string url = "http://192.168.1.71:3000/libros/";
        public async Task<IEnumerable<Libro>> GetAll()
        {
            HttpClient client = new HttpClient();
            string result = await client.GetStringAsync(url);
            return JsonConvert.DeserializeObject<IEnumerable<Libro>>(result);
        }

        public async Task<Libro> Add(string titulo, string descripcion, string autor, string editorial)
        {
            Libro libro = new Libro()
            {
                Titulo = titulo,
                Descripcion = descripcion,
                Autor = autor,
                Editorial = editorial
            };

            HttpClient client = new HttpClient();


            var response = await client.PostAsync(url,
                new StringContent(
                    JsonConvert.SerializeObject(libro),
                    Encoding.UTF8, "application/json"));

            return JsonConvert.DeserializeObject<Libro>(
                await response.Content.ReadAsStringAsync());


        }



    }
}
