﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using GestionLibros.Data;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GestionLibros
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DeleteLibro : ContentPage
    {
        private LibroManager manager;
        public DeleteLibro(LibroManager manager)
        {
            InitializeComponent();
            this.manager = manager;
        }

        public async void OnDeleteLibro(object sender, EventArgs e)
        {

            HttpClient client = new HttpClient();

            var response = await client.DeleteAsync(string.Concat("http://192.168.1.71:3000/libros/", intId.Text));
            await response.Content.ReadAsStringAsync();
            await DisplayAlert("Exito", "Tu libro se ha eliminado", "Ok");
        }

    }
}