﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GestionLibros.Data;
using Xamarin.Forms;

namespace GestionLibros
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]

    public partial class MainPage : ContentPage
    {
        private IList<Libro> libros = new ObservableCollection<Libro>();
        private LibroManager manager = new LibroManager();

        public MainPage()
        {
            BindingContext = libros;
            InitializeComponent();
            Refresh();
        }

        async void OnRefresh(object sender, EventArgs e)
        {
            Refresh();
        }

       

        async private void Refresh()
        {
            var librosCollection = await manager.GetAll();
            foreach (Libro libro in librosCollection)
            {
                if (libros.All(t => t.Id != libro.Id))
                {
                    libros.Add(libro);
                }
            }


        }

        async private void OnAddLibro(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new AddLibro(manager));
        }

        async private void OnUpdateLibro(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new UpdateLibro(manager));
        }

        async private void OnDeleteLibro(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new DeleteLibro(manager));
        }
    }
}
