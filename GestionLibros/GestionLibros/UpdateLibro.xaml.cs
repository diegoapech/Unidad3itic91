﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using GestionLibros.Data;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GestionLibros
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdateLibro : ContentPage
    {
        private LibroManager manager;
        public UpdateLibro(LibroManager manager)
        {
            InitializeComponent();
            this.manager = manager;
        }

        public async void OnUpdateLibro(object sender, EventArgs e)
        {
            Libro libro = new Libro()
            {
                Id = Convert.ToInt32(intId.Text),
                Titulo = txtTitulo.Text,
                Descripcion = txtDescripcion.Text,
                Autor = txtAutor.Text,
                Editorial = txtEditorial.Text,
            };

            HttpClient client = new HttpClient();

            var response = await client.PutAsync(string.Concat("http://192.168.1.71:3000/libros/", intId.Text),
                new StringContent(
                    JsonConvert.SerializeObject(libro),
                    Encoding.UTF8, "application/json"));

            await DisplayAlert("Actualizado", "Tu libro se ha actualizado", "Ok");
        }
    }
}